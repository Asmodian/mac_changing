#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <unistd.h>
#include <stdlib.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <ncurses.h>

#define ETHER_ADDR_LEN 6
#define IP_ADDR_LEN 4
#define IF_BUF_LEN 512
#define X_WINDOW 65
#define Y_WINDOW 30

/*if_name - имя интерфейса, mac - строка в которую будет записан mac-адрес*/
int getMac(char* if_name, unsigned char* mac);
/*ifc - массив, в котором через ; будут указаны все интерфейсы*/
int getInterfaces(char* ifs);
/*if_name - имя интерфейса, mac - строка с mac-адресом*/
int setMac(char *if_name, unsigned char *mac);
/*if_name - имя интерфейса, ip - строка в которую будет записан ip-адрес*/
int getIP(char* if_name, unsigned char* ip);
/*if_name - имя интерфейса, ip - строка с ip-адресом, mask - с маской*/
int setIP(char* if_name, unsigned char* ip, unsigned char* mask);
/*получаем номер интерфейса*/
int indexInterface(char * if_name);
/*возвращает номер строки с которой можно продолжить запись*/
int outputInterfaces(unsigned char *ip, unsigned char *mac, int row);

int main(void)
{
	
	WINDOW *win;
	initscr ();
	noecho();
	win = subwin (stdscr, Y_WINDOW, X_WINDOW, 1, 1);
	box(win, '+', '+');
	move(2, 5);
	addstr("Programm for IP and MAC changing");
	
	int i = 3;
	char type[3];
	char if_name[IF_BUF_LEN];
	unsigned char* mac;
	mac = (unsigned char*)malloc(ETHER_ADDR_LEN);
	unsigned char* ip;
	ip = (unsigned char*)malloc(IP_ADDR_LEN);	
	
	i = outputInterfaces(ip, mac, i);
	i++;
	
	while(indexInterface(if_name) < 0)
	{
		move(i, 2);
		addstr("Input interface for changing:           ");
		move(i, 32);
		echo();
		getstr(if_name);
		noecho();
	}
	i++;
	
	while(strncmp(type, "mac", 3) && strncmp(type, "ip4", 3))
	{
		move(i, 2);
		addstr("What would you like to change?(mac/ip4):           ");
		move(i, 43);
		echo();
		getstr(type);
		noecho();
	}
	i++;
	
	if(!strncmp(type, "mac", 3))
	{
		char mac_address[17];
		move(i, 2);
		addstr("Input new mac-address:           ");
		move(i, 24);
		echo();
		getstr(mac_address);
		noecho();
		
		int j = 0;
		unsigned int mac_number;
		char *mac_symbol, *last;
		mac_symbol = strtok_r(mac_address, ":", &last);
		while(mac_symbol != NULL)
		{			
			mac_number = (unsigned int)strtol(mac_symbol, NULL, 16);
			mac[j] = mac_number;			
			mac_symbol = strtok_r(NULL, ":", &last);
			j++;
		}
		setMac(if_name, mac);
	}
	if(!strncmp(type, "ip4", 3))
	{
		char ip_address[15], ip_mask[15];
		move(i, 2);
		addstr("Input new ip-address:           ");
		move(i, 23);
		echo();
		getstr(ip_address);
		noecho();
		i++;
		move(i, 2);
		addstr("Input new ip-mask:           ");
		move(i, 20);
		echo();
		getstr(ip_mask);
		noecho();
		setIP(if_name, ip_address, ip_mask);
	}
	
	i = i + 2;
	i = outputInterfaces(ip, mac, i);
	
	getch();
	endwin();
	
	free(mac);
	free(ip);
	return 0;
}

int getMac(char* if_name, unsigned char* mac)
{
	int sock, i;
	struct ifreq ifr;
	size_t if_name_len = strlen(if_name);
	
	if(if_name_len < sizeof(ifr.ifr_name)) 
	{
		memcpy(ifr.ifr_name, if_name, if_name_len);
		ifr.ifr_name[if_name_len] = 0;
	} 
	else  
		return -1;
	
	if((sock = socket(AF_INET,SOCK_DGRAM,0)) < 0)
		return -1;
	
	if(ioctl(sock, SIOCGIFHWADDR, &ifr) < 0) 
	{
		close(sock);
		return -1;
	}
	close(sock);
	
	if(ifr.ifr_hwaddr.sa_family != ARPHRD_ETHER)
		return -1;

	for(i = 0; i < ETHER_ADDR_LEN; i++)
		*(mac + i) = *(ifr.ifr_hwaddr.sa_data + i);

	return 0;
}

int getInterfaces(char* ifs)
{
	int i, family;
	struct ifaddrs *ifaddr, *ifa;
	
	if (getifaddrs(&ifaddr) == -1)
		return -1;

	for(ifa = ifaddr, i = 0; ifa != NULL; ifa = ifa->ifa_next, i++)
	{
		if(ifa->ifa_addr == NULL)
			return 0;
		
		family = ifa->ifa_addr->sa_family;
		if(family == AF_PACKET)
		{
			strcat(ifs, ifa->ifa_name);
			strcat(ifs, ";");
		}
	}
	return 0;
}

int setMac(char *if_name, unsigned char *mac)
{
    int sock, i;
    struct ifreq ifr;
    size_t if_name_len = strlen(if_name);
    
	if(if_name_len < sizeof(ifr.ifr_name)) 
	{
		memcpy(ifr.ifr_name, if_name, if_name_len);
		ifr.ifr_name[if_name_len] = 0;
	} 
	else
		return -1;

    if((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        return -1;

    for(i = 0; i < ETHER_ADDR_LEN; i++)
        ifr.ifr_hwaddr.sa_data[i] = mac[i];
        
    ifr.ifr_hwaddr.sa_family = ARPHRD_ETHER;

    if(ioctl(sock, SIOCSIFHWADDR, &ifr)) 
    {
        close(sock);
        return -1;
    }

    close(sock);
    return 0;
}

int getIP(char* if_name, unsigned char* ip)
{
	int sock, i;
	struct ifreq ifr;
	size_t if_name_len = strlen(if_name);
	struct sockaddr_in* addr = (struct sockaddr_in*)&ifr.ifr_addr;
	
	if(if_name_len < sizeof(ifr.ifr_name)) 
	{
		memcpy(ifr.ifr_name, if_name, if_name_len);
		ifr.ifr_name[if_name_len] = 0;
	}
	else 
		return -1;
	
	if((sock = socket(AF_INET,SOCK_DGRAM,0)) < 0)
		return -1;
	
	if(ioctl(sock, SIOCGIFADDR, &ifr) < 0) 
	{ 
		close(sock);
		return -1;
	}
	close(sock);
	
	if(ifr.ifr_addr.sa_family != AF_INET)
		return -1;

	inet_ntop(AF_INET, &addr->sin_addr, ip, INET_ADDRSTRLEN);

	return 0;
}

int setIP(char* if_name, unsigned char* ip, unsigned char* mask)
{
    int sock, i;
    struct ifreq ifr;
    size_t if_name_len = strlen(if_name);
    struct sockaddr_in* addr = (struct sockaddr_in*)&ifr.ifr_addr;
    
	if(if_name_len < sizeof(ifr.ifr_name)) 
	{
		memcpy(ifr.ifr_name, if_name, if_name_len);
		ifr.ifr_name[if_name_len] = 0;
	} 
	else 
		return -1;

    if((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0) 
        return -1;
        
    ifr.ifr_addr.sa_family = AF_INET;

	inet_pton(AF_INET, ip, &addr->sin_addr);
    if(ioctl(sock, SIOCSIFADDR, &ifr)) 
    {
        close(sock);
        return -1;
    }
    
    inet_pton(AF_INET, mask, &addr->sin_addr);
    if(ioctl(sock, SIOCSIFNETMASK, &ifr)) 
    { 
        close(sock);
        return -1;
    }

    close(sock);
    return 0;
}

int indexInterface(char * if_name)
{
	int sock, i;
    struct ifreq ifr;
    size_t if_name_len = strlen(if_name);
    struct sockaddr_in* addr = (struct sockaddr_in*)&ifr.ifr_addr;
    
	if(if_name_len < sizeof(ifr.ifr_name)) 
	{
		memcpy(ifr.ifr_name, if_name, if_name_len);
		ifr.ifr_name[if_name_len] = 0;
	} 
	else
		return -1;

    if((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        return -1;
	
    if (ioctl(sock, SIOCGIFINDEX, &ifr) == -1) {
        return -1;
    }
    if(ifr.ifr_ifindex < 0)
		return -1;
	else
		return 0;
}

int outputInterfaces(unsigned char *ip, unsigned char *mac, int row)
{
	char *ifs, *interface, *last;;
	ifs = (char*)malloc(IF_BUF_LEN);
	getInterfaces(ifs);
	interface = strtok_r(ifs, ";", &last);
	while(interface != NULL) 
	{
		move(row, 2);
		printw("%s", interface);
		memcpy(mac, " ", ETHER_ADDR_LEN);
		memcpy(ip, " ", IP_ADDR_LEN);
		getMac(interface, mac);
		move(row, 17);
		printw("%02X:%02X:%02X:%02X:%02X:%02X\n", mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
		getIP(interface, ip);
		move(row, 42);
		printw("%s", ip);
		row++;
		interface = strtok_r(NULL, ";", &last);
	}
	free(ifs);
	return row;
}
